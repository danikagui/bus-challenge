package com.example.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.springframework.stereotype.Service;

import com.example.BusRoutes;

@Service
@Singleton
public class BusRouteService {

	@Inject	
	private BusRoutes busRoutes;
	
	public boolean hasRoute(Integer depSid, Integer arrSid){
		if(depSid!=null && arrSid!=null){
			int[][] values = busRoutes.getRoutes();
			
			for (int c = 0; c < busRoutes.getRoutesNumber(); c++) {
				final int column = c;
				List<Integer> oCol = Arrays.stream(values).map(o -> o[column]).collect(Collectors.toList());
				
				int depIndex=oCol.indexOf(depSid);
				int arrIndex=oCol.indexOf(arrSid);
				
				if(depIndex > -1 && arrIndex > -1 && depIndex < arrIndex){
					return true;
				}
				
			}
		}
		return false;
	}
	
}
