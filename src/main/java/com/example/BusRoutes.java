package com.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

@Component
public class BusRoutes {
	@Value("${bus_route_file}")
	private String filePath;
	
	private int[][] routes;
	
	private int routesNumber;
	
	private String processedFilemd5;
	
	
	public int getRoutesNumber() {
		return routesNumber;
	}


	public int[][] getRoutes(){
		if(filePath!=null){
			File file = new File(filePath);
			if(file.exists() && file.isFile() && fileHasChanged(file)){
				try (Stream<String> stream = Files.lines(Paths.get(file.toURI()))) {
					
					Iterator<String> it = stream.iterator();
					if(it.hasNext()){
						routesNumber = Integer.valueOf(it.next());
						if(routesNumber > 0){
							int c =0;
							routes = new int[1000][routesNumber];
							while(it.hasNext()){
								
								int[] busRoute = Arrays
										.stream(it.next().split(" "))
										.map(String::trim)
										.mapToInt(Integer::parseInt).toArray();
								for (int r = 0; r < busRoute.length; r++) {
									routes[r][c] = busRoute[r];
								}
								c++;
							}
						}
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return routes;
	}
	
	private boolean fileHasChanged(File file){
		try(FileInputStream fis = new FileInputStream(file)){
			String currentFileMD5 = DigestUtils.md5DigestAsHex(fis);
			if(this.processedFilemd5==null || !this.processedFilemd5.equals(currentFileMD5)){
				this.processedFilemd5 = currentFileMD5;
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}
