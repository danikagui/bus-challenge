package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Init spring boot class
 */
@SpringBootApplication
public class BusRouteApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusRouteApplication.class, args);
	}
}
