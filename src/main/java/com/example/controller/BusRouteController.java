package com.example.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.springframework.stereotype.Component;

import com.example.service.BusRouteService;

@Component
@Path("/api")
public class BusRouteController {
	
	@Inject
	private BusRouteService service;
	
	@GET
	@Path("/direct")
	@Produces("application/json")
	public Map<String, Object> getBusRoute(@QueryParam("dep_sid") Integer depSid,@QueryParam("arr_sid") Integer arrSid){
		Map<String, Object> result = new HashMap<>();
		result.put("dep_sid", depSid);
		result.put("arr_sid", arrSid);
		Boolean hasRoute = service.hasRoute(depSid, arrSid); 
		result.put("direct_bus_route", hasRoute);
		
		return result;
	}
	
}
