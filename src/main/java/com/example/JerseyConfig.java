package com.example;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.example.controller.BusRouteController;

/**
 * Spring JAX-RS support 
 */
@Configuration
public class JerseyConfig extends ResourceConfig{
	public JerseyConfig() {
		register(BusRouteController.class);
	}
}
