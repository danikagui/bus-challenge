package com.example;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class BusRouteApplicationTests {
	@Inject
    private TestRestTemplate restTemplate;
	
	private JacksonTester<Map<String,Object>> json;
	
	
	@LocalServerPort
	private int port;
	
	public BusRouteApplicationTests() {
		File example = new File("src/test/resources/example");
		System.setProperty("bus_route_file", example.getAbsolutePath());
	}
    @Before
    public void setup() {
        ObjectMapper objectMapper = new ObjectMapper();
        JacksonTester.initFields(this, objectMapper);
    }
	
    private String getBaseUrl(){
    	return "http://localhost:" + port + "/";
    }
    
	@Test
	public void getBusRoute() throws IOException {
		int depSid=3;
		int arrSid=6;
		URI url= UriComponentsBuilder.fromUriString(this.getBaseUrl())
			    .path("/api/direct")
			    .queryParam("dep_sid", depSid)
			    .queryParam("arr_sid", arrSid)
			    .build()
			    .toUri();
		String body = this.restTemplate.getForObject(url, String.class);
		assertThat(body).isNotNull();
		Map<String,Object> values =this.json.parseObject(body);
		assertThat(values.get("dep_sid")).isEqualTo(depSid);
		assertThat(values.get("arr_sid")).isEqualTo(arrSid);
		assertThat(values.get("direct_bus_route")).isEqualTo(true);
	}    	
}


